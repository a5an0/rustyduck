use rusoto_core::region::Region;
use rusoto_s3::{S3Client, S3};

use std::fs;
use std::fs::File;
use std::io;

pub fn list_buckets(region: Region) -> Result<Vec<String>, String> {
    let client = S3Client::new(region);
    match client.list_buckets().sync() {
        Ok(res) => match res.buckets {
            Some(buckets) => Ok(buckets
                .iter()
                .map(|bucket| match &bucket.name {
                    Some(name) => name.clone(),
                    None => "".to_owned(),
                })
                .collect()),
            None => Err("No buckets found".to_owned()),
        },
        Err(err) => Err(format!("Error getting list of buckets: {:?}", err)),
    }
}

pub fn list_objects(
    region: Region,
    bucket: &str,
    prefix: Option<String>,
) -> Result<Vec<String>, String> {
    let client = S3Client::new(region);
    match client
        .list_objects_v2(rusoto_s3::ListObjectsV2Request {
            bucket: String::from(bucket),
            prefix: prefix,
            //delimiter: Some("/".to_string()),
            ..Default::default()
            // continuation_token: None,
            // delimiter: None,
            // encoding_type: None,
            // fetch_owner: None,
            // max_keys: None,
            // prefix: None,
            // request_payer: None,
            // start_after: None,
        })
        .sync()
    {
        Ok(output) => match output.contents {
            Some(contents) => Ok(contents
                .iter()
                .map(|item| match &item.key {
                    Some(name) => name.clone(),
                    None => "".to_owned(),
                })
                .collect()),
            None => Err("Bucket appears to be empty".to_owned()),
        },
        Err(err) => Err(format!("Error listing bucket: {}", err)),
    }
}

pub fn get_object(region: Region, bucket: &str, key: &str, filename: &str) -> Result<u64, String> {
    let client = S3Client::new(region);
    match client
        .get_object(rusoto_s3::GetObjectRequest {
            bucket: bucket.to_string(),
            key: key.to_string(),
            ..Default::default()
        })
        .sync()
    {
        Ok(output) => match output.body {
            Some(body) => match File::create(filename) {
                Ok(mut f) => match io::copy(&mut body.into_blocking_read(), &mut f) {
                    Ok(size) => Ok(size),
                    Err(e) => Err(e.to_string()),
                },
                Err(err) => Err(format!("Could not open local file to write: {}", err)),
            },
            None => Err("Could not read object body".to_string()),
        },
        Err(err) => Err(format!("Could not download object: {}", err)),
    }
}

pub fn put_object(region: Region, bucket: &str, key: &str, filename: &str) -> Result<(), String> {
    match fs::read(filename) {
        Ok(f) => {
            let client = S3Client::new(region);
            let _ = client
                .put_object(rusoto_s3::PutObjectRequest {
                    bucket: bucket.to_string(),
                    key: key.to_string(),
                    body: Some(rusoto_s3::StreamingBody::from(f)),
                    ..Default::default()
                })
                .sync();
            Ok(())
        }
        Err(err) => Err(format!("Could not open file {}: {}", filename, err)),
    }
}
