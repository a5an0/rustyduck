extern crate rusoto_s3;

extern crate clap;

use clap::{App, AppSettings, Arg, SubCommand};
use rusoto_core::Region;
use std::str::FromStr;

mod operations;

fn main() {
    let matches = App::new("Rusty Duck")
        .version("0.1")
        .about("Do fun things with S3")
        .arg(
            Arg::with_name("region")
                .short("r")
                .long("region")
                .help("Set the AWS region to connect to")
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("ls")
                .about("List buckets and objects")
                .version("0.1")
                .arg(
                    Arg::with_name("PATH")
                        .help("Optional path prefix to list")
                        .required(false)
                        .index(1),
                ),
        )
        .subcommand(
            SubCommand::with_name("get")
                .about("Download an object")
                .version("0.1")
                .arg(
                    Arg::with_name("OBJECT_PATH")
                        .help("Path to the object")
                        .required(true)
                        .index(1),
                )
                .arg(
                    Arg::with_name("FILENAME")
                        .help("Filename to write the object to")
                        .required(true)
                        .index(2),
                ),
        )
        .subcommand(
            SubCommand::with_name("put")
                .about("Put an object")
                .version("0.1")
                .arg(
                    Arg::with_name("FILENAME")
                        .help("Filename to read")
                        .required(true)
                        .index(1),
                )
                .arg(
                    Arg::with_name("OBJECT_PATH")
                        .help("Path to the object")
                        .required(true)
                        .index(2),
                ),
        )
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();
    let region = match matches.value_of("region") {
        Some(r) => Region::from_str(r).unwrap(),
        None => Region::default(),
    };
    match matches.subcommand() {
        ("ls", Some(matches)) => {
            if let Some(path) = matches.value_of("PATH") {
                let split_prefix: Vec<&str> = path.split("/").collect();
                let bucket = split_prefix[0];
                let prefix = {
                    if split_prefix.len() > 1 {
                        Some(split_prefix[1..].join("/"))
                    } else {
                        None
                    }
                };
                match operations::list_objects(region, bucket, prefix) {
                    Ok(items) => items.iter().for_each(|item| {
                        println!("{}", item);
                    }),
                    Err(err) => println!("Error listing items: {}", err),
                }
            } else {
                match operations::list_buckets(region) {
                    Ok(buckets) => buckets.iter().for_each(|bucket| {
                        println!("{}", bucket);
                    }),
                    Err(err) => println!("Error listing buckets: {}", err),
                }
            }
        }
        ("get", Some(matches)) => {
            let path = matches.value_of("OBJECT_PATH").unwrap();
            let filename = matches.value_of("FILENAME").unwrap();
            let split_prefix: Vec<&str> = path.split("/").collect();
            let bucket = split_prefix[0];
            let key = split_prefix[1..].join("/");
            match operations::get_object(region, bucket, &key, filename) {
                Ok(size) => println!("Wrote {} bytes to {}", size, filename),
                Err(err) => println!("{}", err),
            }
        }
        ("put", Some(matches)) => {
            let path = matches.value_of("OBJECT_PATH").unwrap();
            let filename = matches.value_of("FILENAME").unwrap();
            let split_prefix: Vec<&str> = path.split("/").collect();
            let bucket = split_prefix[0];
            let key = split_prefix[1..].join("/");
            match operations::put_object(region, bucket, &key, filename) {
                Ok(()) => println!("Wrote to {}", path),
                Err(err) => println!("{}", err),
            }
        }
        (&_, _) => {} // the catch-all case
    }
}
